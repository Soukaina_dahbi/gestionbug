package gestionbug.demo.service;

import gestionbug.demo.domain.Projet;
import gestionbug.demo.domain.User;
import gestionbug.demo.repo.ProjetRepository;
import gestionbug.demo.repo.UserRepository;
import org.apache.catalina.realm.X509UsernameRetriever;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProjetService {
    @Autowired
    private ProjetRepository projetRepository;

    @Autowired
    private UserRepository userRepository;




    public List<Projet> getAll() {

        return  projetRepository.findAll();
    }
    public Projet add(Projet projet){

       // if(projet.getId()==null)
        {
            //List<User> users = new ArrayList();
          //  for (int i = 0; i< projet.getUsers().size();i++){
           //     Optional<User> us = userRepository.findById(projet.getUsers().get(i).getId());
             //   if(us.isPresent()){
             //       users.add(us.get());
                }
           // }
          //  projet.setUsers(users);
           // return projetRepository.save(projet);
       // }
        //System.out.println(projet.getId());
       // return null;
        if (projet.getId() == null)
            return projetRepository.save(projet);
        return null;
    }

    public Optional<Projet> findOne(Long id){
        return  projetRepository.findById(id);
    }

    public Projet find(Long id){
        Optional<Projet> projet=findOne(id);
        if (projet.isPresent()){
            return projet.get();
        }
        return null;
    }

    public void  delete(Long id){
         Projet projet=find(id);
        if (projet!=null)
            projetRepository.delete(projet);
    }
    public List<Projet> search(String nom){
        return projetRepository.findBynom(nom);
    }



    public Projet edit(Projet projet){
        if (projet.getId()==0 || find(projet.getId())==null) {
            return projet;
        }
        return  projetRepository.save(projet);
    }



}
