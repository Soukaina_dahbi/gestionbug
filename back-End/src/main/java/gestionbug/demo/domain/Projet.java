package gestionbug.demo.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Entity
public class Projet {

	@Id
	@GeneratedValue
	private Long Id;
	private String nom;
	private Date date;
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable( 	name = "projects_users",joinColumns = @JoinColumn(name = "project_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"))
	private List<User> users;

	@OneToMany(mappedBy = "projet")
	private List<Bug> bug;

	public Projet(Long id, String nom, Date date, List<User> users, List<Bug> bug) {
		Id = id;
		this.nom = nom;
		this.date = date;
		this.users = users;
		this.bug = bug;
	}

	public Projet() {
	}

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public List<Bug> getBug() {
		return bug;
	}

	public void setBug(List<Bug> bug) {
		this.bug = bug;
	}

}
