package gestionbug.demo.repo;

import gestionbug.demo.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserRepository extends JpaRepository<User,Long> {

    @Query( nativeQuery = true,  value = "SELECT s.nom from user s,projects_users p WHERE s.id=p.user_id and project_id =:id")
    List<User> returnList(@Param("id") Long id);

    //List<User> findUserByProjet(Long id );



}
