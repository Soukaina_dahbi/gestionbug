package gestionbug.demo.rest;

import gestionbug.demo.domain.User;
import gestionbug.demo.repo.UserRepository;
import gestionbug.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("user")
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    UserRepository userRepository;

    @PostMapping("")
    public User add(@RequestBody User user)
    {
        return userService.add(user);
    }

    @GetMapping("/find/{id}")
    public User find(@PathVariable Long id){return  userService.find(id);}

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) { userService.delete(id);}

    @PutMapping("")
    public User edite(@RequestBody User user){ return userService.edit(user);}




    @GetMapping("/list/{id}")
    public List<User> findUserList(@PathVariable Long id){
        return userRepository.returnList(id);
    }

//    @GetMapping("/all{id}")
//    public List<User> findALl(Long id){
//        return userRepository.findUserByProjet(id);
//    }


    @GetMapping("")
    @ResponseBody
    public List<User> getAll() {
        return userService.getAll();
    }

}
