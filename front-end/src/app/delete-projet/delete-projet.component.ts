import { Component, OnInit, Inject } from '@angular/core';
import { ProjetServiceService } from 'src/app/Service/projet/projet-service.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Projet } from '../models/projet';
@Component({
  selector: 'app-delete-projet',
  templateUrl: './delete-projet.component.html',
  styleUrls: ['./delete-projet.component.css']
})
export class DeleteProjetComponent implements OnInit {
  projets:Projet[];
  idProjet:number;
  projet:Projet;
  nom:string;
  constructor(private dialogRef: MatDialogRef<DeleteProjetComponent>,private dialog: MatDialog,private projetService:ProjetServiceService) { }
@Inject(MAT_DIALOG_DATA)
public data:any;
  ngOnInit() {
  }
 
 
  cancel(){
    this.dialogRef.close();

  }
  deleteProjet(){
    this.projetService.deleteProjes(this.data.id).subscribe(
      data=>{
        this.cancel();
      }
    )
  }
  // getAllProjets(){
  //   this.projetService.getAllProje().subscribe((projets:Projet[])=>{
  //     this.projets= projets;
  //         })
  // }
  // deleteProjet(id){
  //   this.projetService.deleteProjes(id).subscribe(data=>{
  //   this.getAllProjets();
  //   })
  // }
}
