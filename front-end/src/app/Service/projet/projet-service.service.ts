import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Projet } from '../../models/projet';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProjetServiceService {

  constructor(private http:HttpClient) { }
  getAllProje(){
    return this.http.get('http://localhost:9090/api/projet')
  }
  getOneProje(id:number){
    // return this.http.get('http://localhost:8081/api/projet/'+id)
    return this.http.get(`http://localhost:9090/api/projet/find/${id}`)
  }

  addProje(projet:Projet){
    return this.http.post('http://localhost:9090/api/projet',projet)
    
  }
  updateProjes(projet:Projet){
    return this.http.put('http://localhost:9090/api/projet',projet)
  }
  deleteProjes(id:number):Observable<any>{
    return this.http.delete(`http://localhost:9090/api/projet/${id}`,{responseType:'text'})
  }
}
