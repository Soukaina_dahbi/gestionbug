import {Component, OnInit, ViewChild, EventEmitter, Output} from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator, MatDialog } from '@angular/material';
import { UserServiceService } from 'src/app/Service/User/user-service.service';
import { User } from 'src/app/models/user';
import { MyDialogComponent } from 'src/app/my-dialog/my-dialog.component';
import { SupprimerUserComponent } from '../supprimer-user/supprimer-user.component';


@Component({
  selector: 'app-grid-user',
  templateUrl: './grid-user.component.html',
  styleUrls: ['./grid-user.component.css']
})
export class GridUserComponent implements OnInit {
  pageActual:number=1;

  @Output() addUserEvent: EventEmitter<any> = new EventEmitter()
  users: User[] = [];
  firstname:String;

  constructor(
  
    public dialog: MatDialog,
    private userService:UserServiceService
  ) {}
  Search(){
    if(this.firstname !=""){
      this.users=this.users.filter(res=>{
        return res.nom.toLocaleLowerCase().match(this.firstname.toLocaleLowerCase())
      });
    }else if(this.firstname == ""){
      this.ngOnInit();
    }
   
  }

  ngOnInit() {
    this.userService.getAllUser().subscribe(res => {
      this.users = res as User[];
    });
  }
 
  
  onEditUser(user: User) {
    let dialogRef = this.dialog.open(MyDialogComponent, {
      data: user
    });
  } 
   
  openDialog(id:any): void {
    
    const dialogRef = this.dialog.open(SupprimerUserComponent, {
        
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.onDeleteUser(id)
    }); 
  }




  onDeleteUser(id: number) {
    this.userService.deleteUser(id)
      .subscribe(
        response => {
          this.users = this.users.filter( 
            (item) => item.id != id 
          );
        }
      )
  }

  getDate(){
    this.ngOnInit();
  }

}



