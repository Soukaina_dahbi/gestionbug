import { Component, OnInit, Output ,EventEmitter} from '@angular/core';
import { UserServiceService } from 'src/app/Service/User/user-service.service';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-supprimer-user',
  templateUrl: './supprimer-user.component.html',
  styleUrls: ['./supprimer-user.component.css']
})
export class SupprimerUserComponent implements OnInit {

  constructor(
    private dialogRef: MatDialogRef<SupprimerUserComponent>,
    private userService:UserServiceService) { }
  ngOnInit() {
    }

  test()
  {
    this.dialogRef.close();
  }
  
  
}

