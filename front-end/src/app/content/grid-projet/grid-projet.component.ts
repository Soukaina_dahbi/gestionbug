import { Component, OnInit, Output,EventEmitter } from '@angular/core';
import { AddProjetComponent } from 'src/app/add-projet/add-projet.component';
import { MatDialog } from '@angular/material';
import { ProjetServiceService } from 'src/app/Service/projet/projet-service.service';
import { Projet } from 'src/app/models/projet';
import { EdiProjetComponent } from 'src/app/edi-projet/edi-projet.component';
import { DeleteProjetComponent } from 'src/app/delete-projet/delete-projet.component';

@Component({
  selector: 'app-grid-projet',
  templateUrl: './grid-projet.component.html',
  styleUrls: ['./grid-projet.component.css']
})
export class GridProjetComponent implements OnInit {
  page:number=1;

projets:Projet[];
idProjet:number;
projet:Projet;
nom:string;
  constructor(private dialog: MatDialog,private projetService:ProjetServiceService) { }

  ngOnInit() {
    this.getAllProjets();
  }
  getData(){
    this.ngOnInit();
  }
  getAllProjets(){
    this.projetService.getAllProje().subscribe((projets:Projet[])=>{
      this.projets= projets;
          })
  }
  openDialog(id:number): void {
    let dialogRef = this.dialog.open(AddProjetComponent);
    dialogRef.afterClosed().subscribe(()=>{
      this.getAllProjets();
    })
  }

  openDialogEdit(id:number){
    let dialogRef = this.dialog.open(EdiProjetComponent,{
      data: {id: id}
    });

    dialogRef.afterClosed().subscribe(()=>{
      this.getAllProjets();
    })
  }

  deleteProjet(id){
    this.projetService.deleteProjes(id).subscribe(()=>{
    this.getAllProjets();
    })
  }
  // openDialogDelete(id){
    
  //   const dialogRef = this.dialog.open(DeleteProjetComponent,{
  //     data:{id:id}
  //   });
  //   dialogRef.afterClosed().subscribe(result=>{
  //     this.ngOnInit();
  //   })
  // }
  // openDialogDelete(projet:Projet){
  //   const dialogRef=this.dialog.open(DeleteProjetComponent,{
  //     data:{id:projet.id}
  //   })
  //   dialogRef.afterClosed().subscribe(result=>{
  //     this.getAllProjets();
  //   })
  // }
  Search(){
    if(this.nom !=""){
    this.projets=this.projets.filter(res=>{
    return res.nom.toLocaleLowerCase().match(this.nom.toLocaleLowerCase())
    });
    }else if(this.nom == ""){
    this.ngOnInit();
    }
    }
}
