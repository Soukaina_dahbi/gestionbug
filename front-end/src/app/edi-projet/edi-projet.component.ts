import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Projet } from '../models/projet';
import { ProjetServiceService } from '../Service/projet/projet-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edi-projet',
  templateUrl: './edi-projet.component.html',
  styleUrls: ['./edi-projet.component.css']
})
export class EdiProjetComponent implements OnInit {
id:number;
projet:Projet={
  nom:"",
  date:new Date()
};
  constructor(private projetService:ProjetServiceService ,private router:Router, public dialogRef: MatDialogRef<EdiProjetComponent>,
    @Inject(MAT_DIALOG_DATA) public data) { 
      this.id = data.id
    }

  ngOnInit() {
    this.projetService.getOneProje(this.id).subscribe((projet:Projet)=>{
      this.projet = projet;
    })
  }

  onEditProject(form){
    if(form.valid){
      this.projetService.updateProjes(this.projet).subscribe(()=>{
        this.dialogRef.close();
        this.router.navigate(['/listProjet'])
      })
    }
  }

}
